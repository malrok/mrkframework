package smartalarm.mrk.com.example.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.model.ModelInterface;

import java.util.Date;

public class Tree implements ModelInterface {

	private int identifier;

	private String name;

	private int age;

	private boolean leafy;

	private Date time;

	private Color color;

	public Tree() {
		super();
	}

	private Tree(Parcel in) {
		this.identifier = in.readInt();
		this.name = in.readString();
		this.age = in.readInt();
		this.leafy = in.readByte() != 0;
		this.time = new Date(in.readLong());
		this.color = (Color) in.readSerializable();
	}

	@Override
	public int getIdentifier() {
		return identifier;
	}

	@Override
	public void setIdentifier(int identifier) {
		this.identifier = identifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean isLeafy() {
		return leafy;
	}

	public void setLeafy(boolean leafy) {
		this.leafy = leafy;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public Tree copy() {
		Tree tree = new Tree();

		tree.setIdentifier(this.getIdentifier());
		tree.setName(this.getName());
		tree.setAge(this.getAge());
		tree.setLeafy(this.isLeafy());
		tree.setTime(this.getTime());
		tree.setColor(this.getColor());

		return tree;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeInt(this.identifier);
		parcel.writeString(this.name);
		parcel.writeInt(this.age);
		parcel.writeByte((byte) (this.leafy ? 1 : 0));
		parcel.writeLong(this.time.getTime());
		parcel.writeSerializable(this.color);
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Tree createFromParcel(Parcel in) {
			return new Tree(in);
		}

		public Tree[] newArray(int size) {
			return new Tree[size];
		}
	};
}
