package smartalarm.mrk.com.example.viewmodel;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.viewmodel.abstracts.AbstractViewModel;

import smartalarm.mrk.com.example.model.Tree;

public class TreeViewModel extends AbstractViewModel<Tree> {

	public TreeViewModel() {
		super();
	}

	private TreeViewModel(Parcel in) {
		this.loadFromData((Tree) in.readParcelable(Tree.class.getClassLoader()));
	}

	@Override
	public TreeViewModel copy() {
		TreeViewModel newVM = new TreeViewModel();

		newVM.loadFromData(this.data);

		return newVM;
	}

	public String getStringForAge(Context context) {
		return "age: " + this.data.getAge();
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public TreeViewModel createFromParcel(Parcel in) {
			return new TreeViewModel(in);
		}

		public TreeViewModel[] newArray(int size) {
			return new TreeViewModel[size];
		}
	};
}
