package smartalarm.mrk.com.example.ui.fragments;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.mrk.mrkframework.databinding.interfaces.ChangeListener;
import com.mrk.mrkframework.ui.AbstractMRKFragment;

import java.util.Date;

import smartalarm.mrk.com.example.R;
import smartalarm.mrk.com.example.model.Tree;
import smartalarm.mrk.com.example.ui.MainActivity;
import smartalarm.mrk.com.example.ui.views.TimePicker;

public class TreeFragment extends AbstractMRKFragment<Tree> {

	@Override
	public void onStart() {
		final TextView timeText = (TextView) this.getRootView().findViewById(R.id.timeText);

		TimePicker picker = (TimePicker) this.getRootView().findViewById(R.id.time);

		picker.addChangeListener(new ChangeListener() {
			@Override
			public void onValueChanged(Object newValue, Object oldValue) {
				timeText.setText(((Date) newValue).getHours() + ":" + ((Date) newValue).getMinutes());
			}
		});

		super.onStart();
	}

	@Override
	public int getLayout() {
		return R.layout.tree_fragment;
	}

	@Override
	public void onStop() {
		pushViewModel();
		super.onStop();
	}

	@Override
	public void onValueChanged(View view, String bindValue, Object newValue, Object oldValue) {
		Log.d(this.getClass().getSimpleName(), "new value on view " + view.getClass().getSimpleName() + ": " + newValue);
	}

	private void pushViewModel() {
		((MainActivity)this.getActivity()).getViewModel().getSelectedItem().loadFromData(getViewModel().getData());
	}
}
