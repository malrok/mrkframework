package smartalarm.mrk.com.example.viewmodel;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.dataloaders.interfaces.IDataLoader;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;

import java.util.List;

import smartalarm.mrk.com.example.model.Tree;

public class TreeListViewModel extends AbstractListViewModel<Tree> {

	public TreeListViewModel() {
		super();
	}

	private TreeListViewModel(Parcel in) {
		this.loadFromData(in.readParcelableArray(Tree.class.getClassLoader()), TreeViewModel.class);
	}

	@Override
	public void loadFromDataLoader(Context context, IDataLoader<Tree> dataLoader) {
		clear();

		List<Tree> dataList = dataLoader.getData(context);

		for (Tree data : dataList) {
			TreeViewModel vm = new TreeViewModel();
			vm.loadFromData(data);
			this.getViewModelList().add(vm);
		}
	}

	@Override
	public IListViewModel<Tree> copy() {
		TreeListViewModel trees = new TreeListViewModel();

		for (Tree tree : this.getDataList()) {
			TreeViewModel vm = new TreeViewModel();
			vm.loadFromData(tree.copy());
			trees.getViewModelList().add(vm);
		}

		return trees;
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public TreeListViewModel createFromParcel(Parcel in) {
			return new TreeListViewModel(in);
		}

		public TreeListViewModel[] newArray(int size) {
			return new TreeListViewModel[size];
		}
	};
}
