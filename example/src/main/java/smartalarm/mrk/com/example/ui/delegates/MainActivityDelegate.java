package smartalarm.mrk.com.example.ui.delegates;

import android.app.Activity;
import android.app.Fragment;
import android.widget.Toast;

import com.mrk.mrkframework.ui.fragmentdelegate.AbstractFragmentActivityDelegate;
import com.mrk.mrkframework.ui.fragmentdelegate.WrongFragmentException;

import smartalarm.mrk.com.example.ui.MainActivity;
import smartalarm.mrk.com.example.ui.fragments.TreeFragment;
import smartalarm.mrk.com.example.ui.fragments.TreesListFragment;

public class MainActivityDelegate extends AbstractFragmentActivityDelegate {

	public MainActivityDelegate(Activity activity, int fragmentContainer) {
		super(activity, fragmentContainer);
	}

	@Override
	public void onBackPressed() {
		try {
			if (getCurrentFragment().equals(getFragmentFromClassName(TreesListFragment.class.getName()))) {
				getActivity().finish();
			}
			setPreviousFragment();
			if (getCurrentFragment().equals(getFragmentFromClassName(TreesListFragment.class.getName()))) {
				((TreesListFragment)getCurrentFragment()).setListViewModel(((MainActivity)this.getActivity()).getViewModel());
			}
		} catch (WrongFragmentException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Fragment getFragmentFromAction(String action) {
		Fragment nextFragment = null;

		try {
			switch (MainActivityDelegateActions.Actions.valueOf(action)) {
				case ACTIVITY_RESUME:
					nextFragment = getFragmentFromClassName(TreesListFragment.class.getName());
					((TreesListFragment)nextFragment).setListViewModel(((MainActivity)this.getActivity()).getViewModel());
					break;
				case TREESLIST_SELECT:
					nextFragment = getFragmentFromClassName(TreeFragment.class.getName());
					((TreeFragment)nextFragment).setViewModel(((MainActivity)this.getActivity()).getViewModel().getSelectedItem());
					break;
			}
		} catch (WrongFragmentException e) {
			e.printStackTrace();
			Toast.makeText(this.getActivity().getApplicationContext(), "wrong fragment", Toast.LENGTH_SHORT).show();
		}

		return nextFragment;
	}
}
