package smartalarm.mrk.com.example.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.mrk.mrkframework.ui.AbstractMRKFragmentActivity;
import com.mrk.mrkframework.ui.fragmentdelegate.interfaces.FragmentActivityDelegate;

import smartalarm.mrk.com.example.R;
import smartalarm.mrk.com.example.dataloader.TreeDataLoader;
import smartalarm.mrk.com.example.ui.delegates.MainActivityDelegate;
import smartalarm.mrk.com.example.ui.delegates.MainActivityDelegateActions;
import smartalarm.mrk.com.example.ui.fragments.TreeFragment;
import smartalarm.mrk.com.example.ui.fragments.TreesListFragment;
import smartalarm.mrk.com.example.viewmodel.TreeListViewModel;

public class MainActivity extends AbstractMRKFragmentActivity {

	private TreeListViewModel viewModel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPause() {
		super.onPause();
//        save();
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (this.viewModel == null) {
			load();
		}

		if (getCurrentFragment() == null) {
			sendActionToDelegate(MainActivityDelegateActions.Actions.ACTIVITY_RESUME.toString());
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putParcelable("viewModel", getViewModel());

		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		this.viewModel = savedInstanceState.getParcelable("viewModel");

		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	public Class<? extends Fragment>[] getFragmentsList() {
		return new Class[] {
				TreesListFragment.class,
				TreeFragment.class
		};
	}

	@Override
	public FragmentActivityDelegate setDelegate() {
		return new MainActivityDelegate(this, R.id.fragment_container);
	}

	public TreeListViewModel getViewModel() {
		return viewModel;
	}

	private void save() {
		// TODO...
	}

	private void load() {
		this.viewModel = new TreeListViewModel();
		this.viewModel.loadFromDataLoader(this, new TreeDataLoader());
	}
}
