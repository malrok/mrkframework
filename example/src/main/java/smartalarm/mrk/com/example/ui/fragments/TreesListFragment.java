package smartalarm.mrk.com.example.ui.fragments;

import android.view.View;
import android.widget.AdapterView;

import com.mrk.mrkframework.ui.AbstractMRKRecyclerViewFragment;

import smartalarm.mrk.com.example.R;
import smartalarm.mrk.com.example.model.Tree;
import smartalarm.mrk.com.example.ui.MainActivity;
import smartalarm.mrk.com.example.ui.adapters.TreeListAdapter;
import smartalarm.mrk.com.example.ui.delegates.MainActivityDelegateActions;

public class TreesListFragment extends AbstractMRKRecyclerViewFragment<Tree> {

	@Override
	public int getLayout() {
		return R.layout.trees_list_fragment;
	}

	@Override
	public int getItemLayout() {
		return R.layout.treeslist_item;
	}

	@Override
	public Class<TreeListAdapter> getAdapterClass() {
		return TreeListAdapter.class;
	}

	@Override
	protected boolean copyViewModel() {
		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		super.onItemClick(parent, view, position, id);
		((MainActivity)this.getActivity()).sendActionToDelegate(MainActivityDelegateActions.Actions.TREESLIST_SELECT.toString());
	}
}
