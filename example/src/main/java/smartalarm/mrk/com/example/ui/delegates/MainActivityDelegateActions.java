package smartalarm.mrk.com.example.ui.delegates;

import com.mrk.mrkframework.ui.fragmentdelegate.AbstractDelegateActions;

public class MainActivityDelegateActions extends AbstractDelegateActions {

	public enum Actions {
		/** activity */
		ACTIVITY_RESUME,

		/** TREES LIST FRAGMENT*/
		TREESLIST_SELECT
	}

}
