package smartalarm.mrk.com.example.ui.adapters;

import com.mrk.mrkframework.ui.adapter.AbstractRecyclerViewFragmentAdapter;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;

import smartalarm.mrk.com.example.R;
import smartalarm.mrk.com.example.model.Tree;

public class TreeListAdapter extends AbstractRecyclerViewFragmentAdapter<Tree> {

	public TreeListAdapter(IListViewModel vmList) {
		super(vmList);
	}

	@Override
	public int getItemLayout(int viewType) {
		return R.layout.treeslist_item;
	}
}
