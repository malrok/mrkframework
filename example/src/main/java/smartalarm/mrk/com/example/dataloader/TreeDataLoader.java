package smartalarm.mrk.com.example.dataloader;

import android.content.Context;

import com.mrk.mrkframework.dataloaders.abstracts.AbstractDataLoader;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import smartalarm.mrk.com.example.model.Color;
import smartalarm.mrk.com.example.model.Tree;

public class TreeDataLoader extends AbstractDataLoader<Tree> {

	@Override
	public List<Tree> getData(Context context) {
		List<Tree> trees = new ArrayList<>();

		Tree tree = new Tree();
		tree.setIdentifier(0);
		tree.setName("Hetre");
		tree.setAge(5);
		tree.setLeafy(true);
		tree.setTime(new Date(1436416800)); // 16h00
		tree.setColor(Color.YELLOW);

		trees.add(tree);

		Tree tree2 = new Tree();
		tree.setIdentifier(1);
		tree2.setName("Sapin");
		tree2.setAge(6);
		tree2.setLeafy(false);
		tree2.setTime(new Date(1452984000)); // 20h36
		tree2.setColor(Color.GREEN);

		trees.add(tree2);

		Tree tree3 = new Tree();
		tree.setIdentifier(2);
		tree3.setName("Bouleau");
		tree3.setAge(7);
		tree3.setLeafy(true);
		tree3.setTime(new Date(1421882400)); // 11h58
		tree3.setColor(Color.RED);

		trees.add(tree3);

		return trees;
	}

	@Override
	public void pushFrom(Context context, List<Tree> data) {

	}
}
