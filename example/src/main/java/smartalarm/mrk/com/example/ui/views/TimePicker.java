package smartalarm.mrk.com.example.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.mrk.mrkframework.databinding.interfaces.BindableView;
import com.mrk.mrkframework.databinding.interfaces.ChangeListener;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import smartalarm.mrk.com.example.R;

public class TimePicker extends View implements BindableView {

	private static final String STATE_PARENT = "parent";
	private static final String STATE_ANGLE = "angle";
	private static final String STATE_TEXT_COLOR = "textColor";
	private static final String STATE_CLOCK_COLOR = "clockColor";
	private static final String STATE_DIAL_COLOR = "dialColor";
	private static final String STATE_DISABLE_TOUCH = "disableTouch";

	Paint paint;
	RectF rectF;

	float width;
	float height;
	float min;
	float padding;
	float radius;
	float dialRadius;
	float offset;
	float slopX;
	float slopY;
	float posX;
	float posY;
	float secAngle;

	int startAngle;
	int hour;
	int minutes;
	int tmp;
	int previousHour;
	int textColor = Color.BLACK;
	int clockColor = Color.BLACK;
	int dialColor = Color.BLACK;

	double angle;
	double minuteAngle;
	double hourAngle;
	double degrees;

	boolean isMoving;
	boolean isMovingHour = true;
	boolean isMovingMinutes = false;
	boolean amPm;
	boolean twentyFour;
	boolean disableTouch;
	boolean hands;
	int scale;

	String hStr;
	String mStr;
	String amPmStr;

	Rect amPmStrBounds;

	List<ChangeListener> timeChangedListener;
	Calendar calendar = Calendar.getInstance();

	Date previousTime;

	public TimePicker(Context context) {
		super(context);
		init(context, null);
	}

	public TimePicker(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}

	private void init(Context context, AttributeSet attrs) {
		timeChangedListener = new ArrayList<>();

		angle = (-Math.PI / 2)+.001;

		paint = new Paint();
		paint.setAntiAlias(true);
		paint.setStrokeCap(Paint.Cap.ROUND);
		paint.setTextAlign(Paint.Align.CENTER);

		rectF = new RectF();

		if (attrs != null) {
			TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TimePicker);
			if (typedArray != null) {
				textColor = typedArray.getColor(R.styleable.TimePicker_text_color, Color.BLACK);
				clockColor = typedArray.getColor(R.styleable.TimePicker_clock_color, Color.BLACK);
				dialColor = typedArray.getColor(R.styleable.TimePicker_dial_color, Color.BLACK);
				disableTouch = typedArray.getBoolean(R.styleable.TimePicker_disable_touch, false);
				twentyFour = typedArray.getBoolean(R.styleable.TimePicker_twenty_four, false);
				hands = typedArray.getBoolean(R.styleable.TimePicker_clock_hands, false);
				scale = typedArray.getInteger(R.styleable.TimePicker_clock_scale, 12);
				typedArray.recycle();
			}
		}

		if (scale != 0) {
			secAngle = 360 / scale;
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		width = MeasureSpec.getSize(widthMeasureSpec);
		height = MeasureSpec.getSize(heightMeasureSpec);

		min = Math.min(width, height);
		setMeasuredDimension((int) min, (int) min);

		offset = min * 0.5f;
		padding = min/20;
		radius = min/2-(padding*2);
		dialRadius = radius/5;
		rectF.set(-radius, -radius, radius, radius);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.translate(offset, offset);

		paint.setStrokeWidth(1);
		paint.setStyle(Paint.Style.FILL);
		paint.setAlpha(255);
		paint.setColor(textColor);

		if (!hands) {
			//Rad to Deg
			degrees = (Math.toDegrees(angle) + 90) % 360;
			degrees = (degrees + 360)%360;

			//get Hour
			hour = ((int)degrees/30)%12;
			if (hour == 0) hour = 12;

			//get Minutes
			minutes = ((int)(degrees * 2))%60;
			mStr = (minutes < 10) ? "0"+minutes : minutes+"";

			//get AM/PM
			if ((hour == 12 && previousHour == 11) || (hour == 11 && previousHour == 12)) amPm = !amPm;
			amPmStr = amPm ? "AM" : "PM";

			previousHour = hour;
			// digital display
			paint.setTextSize(min / 5);
			if (twentyFour) {
				tmp = hour;
				if (!amPm) {
					if (tmp < 12) tmp += 12;
				} else {
					if (tmp == 12) tmp = 0;
				}
				hStr = (tmp < 10) ? "0" + tmp : tmp + "";
				canvas.drawText(hStr + ":" + mStr, 0, paint.getTextSize() / 3, paint);
			} else {
				hStr = (hour < 10) ? "0" + hour : hour + "";
				canvas.drawText(hStr + ":" + mStr, 0, paint.getTextSize() / 4, paint);
				paint.setTextSize(min / 10);
				canvas.drawText(amPmStr, 0, paint.getTextSize() * 2, paint);
			}
		} else {
			//get AM/PM
			amPmStr = amPm ? "AM" : "PM";

			paint.setTextSize(min / 10);
			canvas.drawText(amPmStr, 0, paint.getTextSize() * 2, paint);

			//get AM/PM string bounds
			amPmStrBounds = new Rect();
			paint.setTextSize(min / 10);
			paint.getTextBounds(amPmStr, 0, amPmStr.length(), amPmStrBounds);
			amPmStrBounds.offsetTo(-amPmStrBounds.width() / 2, (int) paint.getTextSize() + amPmStrBounds.height() / 2);

			paint.setStyle(Paint.Style.STROKE);
			paint.setStrokeWidth(min / 30);
			paint.setAlpha(100);

			//get Minutes
			//Rad to Deg
			degrees = (Math.toDegrees(minuteAngle) + 90) % 360;
			degrees = (degrees + 360)%360;
			if (isMovingMinutes) {
				degrees += 3;
			}
			degrees -= degrees % 6;

			// clock hands
			canvas.save();
			canvas.rotate((float) degrees + 180, 0, 0);
			canvas.drawLine(0, radius - padding, 0, 0, paint);
			canvas.restore();

			//get Hours
			//Rad to Deg
			degrees = (Math.toDegrees(hourAngle) + 90) % 360;
			degrees = (degrees + 360)%360;
			if (isMovingHour) {
				degrees += 15;
			}
			degrees -= degrees % 30;

			canvas.save();
			canvas.rotate((float) degrees + 180, 0, 0);
			canvas.drawLine(0, radius - padding * 3, 0, 0, paint);
			canvas.restore();
		}

		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(min / 100);
		paint.setColor(clockColor);
		canvas.drawOval(rectF, paint);

		if (scale != 0) {
			startAngle = 180;
			for (tmp = 0; tmp < scale; tmp++) {
				canvas.save();
				canvas.rotate(startAngle, 0, 0);
				canvas.drawLine(0, radius, 0, radius - padding, paint);
				canvas.restore();
				startAngle += secAngle;
			}
		}

		if (!disableTouch) {
			if (!hands) {
				drawDot(canvas, angle, dialRadius);
			} else {
				if (isMovingHour) {
					drawDot(canvas, hourAngle, dialRadius);
				} else {
					drawDot(canvas, minuteAngle, dialRadius);
				}
			}
		}
	}

	private void drawDot(Canvas canvas, double angle, float radius) {
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(dialColor);
		paint.setAlpha(100);

		float[] pos = calculatePointerPosition(angle);
		canvas.drawCircle(pos[0], pos[1], radius, paint);

		paint.setStyle(Paint.Style.FILL);
		paint.setColor(dialColor);
		paint.setAlpha(255);

		pos = calculatePointerPosition(angle);
		canvas.drawCircle(pos[0], pos[1], radius / 3, paint);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (disableTouch) return false;
		getParent().requestDisallowInterceptTouchEvent(true);

		posX = event.getX() - offset;
		posY = event.getY() - offset;

		Date previous = this.previousTime;

		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				float[] pos;
				if (!hands) {
					pos = calculatePointerPosition(angle);
					if (posX >= (pos[0] - dialRadius) && posX <= (pos[0] + dialRadius)
							&& posY >= (pos[1] - dialRadius) && posY <= (pos[1] + dialRadius)) {

						slopX = posX - pos[0];
						slopY = posY - pos[1];
						isMoving = true;
						invalidate();
					} else {
						angle = (float) Math.atan2(posY, posX);
						invalidate();
					}
				} else {
					boolean redraw = false;

					// get pointer position
					if (isMovingHour) {
						pos = calculatePointerPosition(hourAngle);
					} else {
						pos = calculatePointerPosition(minuteAngle);
					}
					// check if it matches
					if (posX >= (pos[0] - dialRadius) && posX <= (pos[0] + dialRadius)
							&& posY >= (pos[1] - dialRadius) && posY <= (pos[1] + dialRadius)) {
						slopX = posX - pos[0];
						slopY = posY - pos[1];
						isMoving = true;
						redraw = true;
					} else
						// check if am pm is touched
						if (amPmStrBounds.contains((int)posX, (int)posY)) {
							amPm = !amPm;
							if (amPm && hour > 12) {
								hour -= 12;
							} else if (!amPm && hour <= 12) {
								hour += 12;
							}
							redraw = true;
							notifyChange(previous);
						}

					// redraw if necessary
					if (redraw) {
						invalidate();
					}
				}
				break;
			case MotionEvent.ACTION_MOVE:
				if (isMoving) {
					if (!hands) {
						angle = (float) Math.atan2(posY - slopY, posX - slopX);

						//Rad to Deg
						degrees = (Math.toDegrees(angle) + 90) % 360;
						degrees = (degrees + 360) % 360;

						//get Hour
						hour = ((int) degrees / 30) % 12;

						//get Minutes
						minutes = ((int) (degrees * 2)) % 60;

						notifyChange(previous);
						invalidate();
					} else {
						if (isMovingMinutes) {
							minuteAngle = Math.atan2(posY - slopY, posX - slopX);

							//Rad to Deg
							degrees = (Math.toDegrees(minuteAngle) + 90) % 360;
							degrees = (degrees + 360) % 360;
							degrees += 3;

							//get Minutes
							minutes = (int) (degrees / 6);
						} else if (isMovingHour) {
							hourAngle = Math.atan2(posY - slopY, posX - slopX);

							//Rad to Deg
							degrees = (Math.toDegrees(hourAngle) + 90) % 360;
							degrees = (degrees + 360) % 360;
							degrees += 15;

							//get Hour
							previousHour = hour;
							hour = (int) (degrees / 30);

							if ((hour == 0 && previousHour == 11) || (hour == 11 && previousHour == 0)) amPm = !amPm;

							if (hour > 12 && amPm) hour -= 12;
						}

						notifyChange(previous);
						invalidate();
					}
				} else {
					getParent().requestDisallowInterceptTouchEvent(false);
					return false;
				}
				break;
			case MotionEvent.ACTION_UP:
				if (hands && isMoving) {
					isMovingHour = !isMovingHour;
					isMovingMinutes = !isMovingMinutes;

					// set minutes and hour angles
					minuteAngle = Math.PI * minutes / 30 - Math.PI / 2;
					hourAngle = Math.PI * hour / 6 - Math.PI / 2;

					invalidate();
				}
			case MotionEvent.ACTION_CANCEL:
				isMoving = false;
				invalidate();
				break;
		}
		return true;
	}

	private void notifyChange(Date previous) {
		for (ChangeListener listener : timeChangedListener) {
			listener.onValueChanged(getTime(), previous);
		}
	}

	private float[] calculatePointerPosition(double angle) {
		float[] pos = new float[2];
		pos[0] = (float) (radius * Math.cos(angle));
		pos[1] = (float) (radius * Math.sin(angle));
		return pos;
	}

	@Override
	protected Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();
		Bundle state = new Bundle();

		state.putParcelable(STATE_PARENT, superState);
		state.putDouble(STATE_ANGLE, angle);
		state.putInt(STATE_CLOCK_COLOR, clockColor);
		state.putInt(STATE_DIAL_COLOR, dialColor);
		state.putInt(STATE_TEXT_COLOR, textColor);
		state.putBoolean(STATE_DISABLE_TOUCH, disableTouch);

		return state;
	}

	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		Bundle savedState = (Bundle) state;
		Parcelable superState = savedState.getParcelable(STATE_PARENT);
		super.onRestoreInstanceState(superState);

		angle = savedState.getDouble(STATE_ANGLE);
		clockColor = savedState.getInt(STATE_CLOCK_COLOR);
		dialColor = savedState.getInt(STATE_DIAL_COLOR);
		textColor = savedState.getInt(STATE_TEXT_COLOR);
		disableTouch = savedState.getBoolean(STATE_DISABLE_TOUCH);
	}

	public void setColor(int color) {
		this.textColor = color;
		this.clockColor = color;
		this.dialColor = color;
		invalidate();
	}

	public void setTextColor(int textColor) {
		this.textColor = textColor;
		invalidate();
	}

	public void setClockColor(int clockColor) {
		this.clockColor = clockColor;
		invalidate();
	}

	public void setDialColor(int dialColor) {
		this.dialColor = dialColor;
		invalidate();
	}

	public void enableTwentyFourHour(boolean twentyFour) {
		this.twentyFour = twentyFour;
		invalidate();
	}

	public void disableTouch(boolean disableTouch){
		this.disableTouch = disableTouch;
	}

	public Date getTime() {
		tmp = hour;
		if (!amPm) {
			if (tmp < 12) tmp += 12;
		} else {
			if (tmp == 12) tmp = 0;
		}

		if (minutes >= 60) {
			minutes -= 60;
		}

		calendar.set(Calendar.MINUTE, minutes);
		calendar.set(Calendar.HOUR_OF_DAY, tmp);

		this.previousTime = calendar.getTime();

		return calendar.getTime();
	}

	public void setTime(Date date) {
		calendar.setTime(date);
		hour = calendar.get(Calendar.HOUR);
		minutes = calendar.get(Calendar.MINUTE);
		amPm = (calendar.get(Calendar.AM_PM) == Calendar.AM);
		if (!hands) {
			degrees = ((hour * 30) + 270) % 360;
			angle = Math.toRadians(degrees);
			degrees = ((double) minutes / 2);
			angle += Math.toRadians(degrees) + .001;
		} else {
			degrees = ((hour * 30) + 270) % 360;
			hourAngle = Math.toRadians(degrees);
			degrees = ((minutes * 6) + 270) % 360;
			minuteAngle = Math.toRadians(degrees);
		}
		notifyChange(date);
		invalidate();
	}

	@Override
	public void addChangeListener(ChangeListener listener) {
		this.timeChangedListener.add(listener);
	}

	@Override
	public void setValue(Object value) {
		if (value instanceof Date) {
			this.setTime((Date) value);
		} else if (value instanceof Calendar) {
			this.setTime(((Calendar)value).getTime());
		} else if (value instanceof Long) {
			this.setTime(new Date((Long) value));
		} else {
			Log.e(this.getClass().getSimpleName(), "wrong data type");
		}
	}
}
