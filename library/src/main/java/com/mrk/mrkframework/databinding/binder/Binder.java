package com.mrk.mrkframework.databinding.binder;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.mrk.mrkframework.R;
import com.mrk.mrkframework.databinding.interfaces.BindableView;
import com.mrk.mrkframework.databinding.interfaces.BindedViewListener;
import com.mrk.mrkframework.databinding.interfaces.ChangeListener;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class Binder {

	public static final int TWO_WAY_BINDING = 1;

	private WeakReference<BindedViewListener> bindedRootView;

	private IViewModel viewmodel;

	private Map<Integer, HashMap<Integer, String>> bindedViews;

	private boolean initializing = false;

	public Binder(BindedViewListener bindedView, Map<Integer, HashMap<Integer, String>> bindedViews) {
		this.bindedRootView = new WeakReference<>(bindedView);
		this.bindedViews = bindedViews;

		bindViews();
	}

	private void bindViews() {
		BindedViewListener rootView = bindedRootView.get();

		if (rootView != null) {
			for (Map.Entry<Integer, HashMap<Integer, String>> viewMap : bindedViews.entrySet()) {
				View view = rootView.getRootView().findViewById(viewMap.getKey());
				if (view != null) {
					bindView(rootView, view, viewMap);
				}
			}
		}
	}

	private void bindView(BindedViewListener rootView, View view, Map.Entry<Integer, HashMap<Integer, String>> viewMap) {
		if (viewMap.getValue().containsKey(R.attr.bindValue)) {
			String bindValue = viewMap.getValue().get(R.attr.bindValue);

			int mode = TWO_WAY_BINDING;

			if (viewMap.getValue().containsKey(R.attr.bindMode)) {
				mode = Integer.parseInt(viewMap.getValue().get(R.attr.bindMode));
			}

			if (mode == TWO_WAY_BINDING) {
				addListenerOnView(rootView, view, bindValue);
			}
		}
	}

	// TODO : remove final
	private void addListenerOnView(final BindedViewListener rootView, final View view, final String bindValue) {
		if (view instanceof EditText) {
			((EditText)view).addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					// nothing to do
				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if (!initializing) {
						setValue(s.toString(), ((EditText) view).getText(), bindValue, rootView, view);
					}
				}

				@Override
				public void afterTextChanged(Editable s) {
					// nothing to do
				}
			});
		} else if (view instanceof SeekBar) {
			((SeekBar) view).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					if (!initializing) {
						setValue(progress, ((SeekBar) view).getProgress(), bindValue, rootView, view);
					}
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// nothing to do
				}

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// nothing to do
				}
			});
		} else if (view instanceof CompoundButton) {
			((CompoundButton) view).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (!initializing) {
						setValue(isChecked, ((CompoundButton) view).isChecked(), bindValue, rootView, view);
					}
				}
			});
		} else if (view instanceof RadioGroup) {
			((RadioGroup)view).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					if (!initializing) {
						if (Binder.this.viewmodel != null) {
							int newIndex = 0;
							for (int i = 0; i < group.getChildCount(); i++) {
								if (group.getChildAt(i).getId() == checkedId) {
									newIndex = i;
								}
							}

							Object value = getValueForField(bindValue);

							if (value instanceof Enum) {
								Enum enumValue = (Enum) value;

								Enum newValue = Enum.valueOf(enumValue.getClass(), (enumValue.getDeclaringClass().getEnumConstants()[newIndex]).toString());

								setValue(newValue, enumValue.getDeclaringClass().getEnumConstants()[newIndex], bindValue, rootView, view);
							}
						}
					}
				}
			});
		} else if (view instanceof BindableView) {
			((BindableView) view).addChangeListener(new ChangeListener() {
				@Override
				public void onValueChanged(Object newValue, Object oldValue) {
					if (!initializing) {
						setValue(newValue, oldValue, bindValue, rootView, view);
					}
				}
			});
		}
	}

	private void setValue(Object newValue, Object oldValue, String bindValue, BindedViewListener rootView, View view) {
		if (this.viewmodel != null) {
			setValueOnField(bindValue, newValue);
			rootView.onValueChanged(view, bindValue, newValue, oldValue);
		}
	}

	private void setValueOnField(String field, Object newValue) {
		try {
			Field methodField = this.viewmodel.getData().getClass().getDeclaredField(field);

			Method setMethod = null;

			try {
				setMethod = this.viewmodel.getData().getClass().getMethod("set" + capitalizeFirstCharacter(field), methodField.getType());
			} catch (NoSuchMethodException e) {
				try {
					setMethod = this.viewmodel.getData().getClass().getMethod("is" + capitalizeFirstCharacter(field), methodField.getType());
				} catch (NoSuchMethodException e1) {
					e1.printStackTrace();
				}
			}

			if (setMethod != null) {
				setMethod.invoke(this.viewmodel.getData(), newValue);
			}
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}

	private Object getValueForField(String field) {
		Object res = null;

		try {
			Method setMethod = null;
			try {
				setMethod = this.viewmodel.getData().getClass().getMethod("get" + capitalizeFirstCharacter(field));
			} catch (NoSuchMethodException e) {
				try {
					setMethod = this.viewmodel.getData().getClass().getMethod("is" + capitalizeFirstCharacter(field));
				} catch (NoSuchMethodException e1) {
					e1.printStackTrace();
				}
			}

			if (setMethod != null) {
				res = setMethod.invoke(this.viewmodel.getData());
			}
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return res;
	}

	public void pushViewModel(IViewModel viewmodel) {
		this.viewmodel = viewmodel;
		BindedViewListener rootView = bindedRootView.get();

		this.initializing = true;

		if (rootView != null) {
			for (Map.Entry<Integer, HashMap<Integer, String>> viewMap : bindedViews.entrySet()) {
				View view = rootView.getRootView().findViewById(viewMap.getKey());
				if (view != null) {
					if (viewMap.getValue().containsKey(R.attr.bindValue)) {
						String bindValue = viewMap.getValue().get(R.attr.bindValue);
						setValueOnView(view, getValueForField(bindValue));
					} else if (viewMap.getValue().containsKey(R.attr.bindMethod)) {
						String bindMethod = viewMap.getValue().get(R.attr.bindMethod);
						try {
							Method method = this.viewmodel.getClass().getDeclaredMethod(bindMethod, Context.class);
							setValueOnView(view, method.invoke(this.viewmodel, view.getContext()));
						} catch (NoSuchMethodException e) {
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

		this.initializing = false;
	}

	private void setValueOnView(View view, Object bindValue) {
		if (view instanceof CompoundButton) {
			((CompoundButton) view).setChecked((Boolean) bindValue);
		} else if (view instanceof SeekBar) {
			((SeekBar) view).setProgress((Integer) bindValue);
		} else if (view instanceof TextView) {
			((TextView) view).setText(String.valueOf(bindValue));
		} else if (view instanceof RadioGroup) {
			((RadioGroup)view).check(((RadioGroup) view).getChildAt(((Enum) bindValue).ordinal()).getId());
		} else if (view instanceof BindableView) {
			((BindableView) view).setValue(bindValue);
		}
	}

	private String capitalizeFirstCharacter(String string) {
		return string.substring(0,1).toUpperCase() + string.substring(1);
	}
}
