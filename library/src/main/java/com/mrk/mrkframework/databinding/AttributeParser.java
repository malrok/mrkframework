package com.mrk.mrkframework.databinding;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.LayoutInflaterFactory;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.mrk.mrkframework.R;

import java.util.HashMap;
import java.util.Map;

/**
 * cf http://stackoverflow.com/questions/14800642/custom-xml-attributes-without-custom-view-in-fragment
 */
public class AttributeParser {

	private AttributeParserFactory mFactory;
	private Map<Integer, HashMap<Integer, String>> mAttributeList;

	private class AttributeParserFactory implements LayoutInflaterFactory {

		@Override
		public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
			String id = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "id");

			if (id != null) {
				// String with the reference character "@", so we strip it to keep only the reference
				id = id.replace("@", "");

				TypedArray libraryStyledAttributeList = context.obtainStyledAttributes(attrs, R.styleable.CustomAttributes_MRKFramework);
				HashMap<Integer, String> libraryViewAttribute = new HashMap<>();
				int i = 0;

				for (int attribute : R.styleable.CustomAttributes_MRKFramework) {
					String attributeValue = libraryStyledAttributeList.getString(i);

					if (attributeValue != null) {
						libraryViewAttribute.put(attribute, attributeValue);
					}

					i++;
				}

				if (!libraryViewAttribute.isEmpty()) {
					mAttributeList.put(Integer.valueOf(id), libraryViewAttribute);
				}

				libraryStyledAttributeList.recycle();
			}

			return null;
		}
	}

	public AttributeParser(){
		mAttributeList = new HashMap<>();
		mFactory = new AttributeParserFactory();
	}

	public void clear() {
		mAttributeList.clear();
	}

	public LayoutInflater getLayoutInflater(LayoutInflater inflater) {
		clear();
		LayoutInflater layoutInflater = inflater.cloneInContext(inflater.getContext());
		LayoutInflaterCompat.setFactory(layoutInflater, mFactory);

		return layoutInflater;
	}

	public void setFactory(LayoutInflater inflater){
		LayoutInflaterCompat.setFactory(inflater.cloneInContext(inflater.getContext()), mFactory);
	}

	public void setViewAttribute(Activity activity) {
		for (Map.Entry<Integer, HashMap<Integer, String>> attribute : mAttributeList.entrySet()) {
			if (activity.findViewById(attribute.getKey()) != null) {
				activity.findViewById(attribute.getKey()).setTag(attribute.getValue());
			}
		}
	}

	public void setViewAttribute(View view) {
		for (Map.Entry<Integer, HashMap<Integer, String>> attribute : mAttributeList.entrySet()) {
			if (view.findViewById(attribute.getKey()) != null) {
				view.findViewById(attribute.getKey()).setTag(attribute.getValue());
			}
		}
	}

	public Map<Integer, HashMap<Integer, String>> getAttributeList() {
		return mAttributeList;
	}

	public void setAttributeList(Map<Integer, HashMap<Integer, String>> attributeList) {
		this.mAttributeList = attributeList;
	}
}
