package com.mrk.mrkframework.databinding.interfaces;

public interface ChangeListener {

	void onValueChanged(Object newValue, Object oldValue);

}
