package com.mrk.mrkframework.databinding.interfaces;

public interface BindableView {

	void addChangeListener(ChangeListener listener);

	void setValue(Object value);

}
