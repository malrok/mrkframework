package com.mrk.mrkframework.databinding.interfaces;

import android.view.View;

public interface BindedViewListener {

	void onValueChanged(View view, String bindValue, Object newValue, Object oldValue);

	View getRootView();

}
