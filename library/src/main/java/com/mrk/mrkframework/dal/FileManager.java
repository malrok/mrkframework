package com.mrk.mrkframework.dal;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.content.Context;
import android.util.Log;

public class FileManager {

	private Context context;

	// "r" : lecture ; "w" : ecriture
	private String openMode = null;
	
	// ecriture
	private FileOutputStream ficWrite = null;
	private OutputStreamWriter strWrite = null;
	
	// lecture
	private InputStream ficRead = null;
	private BufferedReader strRead = null;
	
	public FileManager(Context ctx, String file, String mode) {
		context = ctx;
		openMode = mode;
		if (openMode == "w") {
			try {
				ficWrite = context.openFileOutput(file, Context.MODE_PRIVATE);
				strWrite = new OutputStreamWriter(ficWrite);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}	
		} else {
			try {
				ficRead = context.openFileInput(file);
				strRead = new BufferedReader(new InputStreamReader(ficRead));
			} catch (FileNotFoundException e) {
				try {
					new OutputStreamWriter(context.openFileOutput(file, Context.MODE_PRIVATE));
					ficRead = context.openFileInput(file);
					strRead = new BufferedReader(new InputStreamReader(ficRead));
				} catch (FileNotFoundException e1) {
					Log.e("Error","Could not create preference file");
				}
			}
		}
	}
	
	public void close() {
		if (openMode == "w") {
			try {
				strWrite.flush();
				strWrite.close();
				ficWrite.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				strRead.close();
				ficRead.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public String read() throws IOException {
		if (openMode == "w")
			return null;
		else {
			String content = "", line = "";
			
			while ((line = strRead.readLine()) != null) {
				content += line;
			}
			
			return content;
		}
	}
	
	public String readLine() throws IOException {
		if (openMode == "w")
			return null;
		else {
	        return strRead.readLine();
		}
	}
	       
	public void write(String string) throws IOException {
		if (openMode == "w") {
			strWrite.write(string + "\n");
		}
	}
}
