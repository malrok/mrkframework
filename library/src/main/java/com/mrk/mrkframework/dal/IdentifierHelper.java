package com.mrk.mrkframework.dal;

import java.util.HashMap;
import java.util.Map;

public class IdentifierHelper {

	private static IdentifierHelper instance;

	private Map<Class<?>, Integer> classIdentifiersMap;

	public static IdentifierHelper getInstance() {
		if (instance == null) {
			instance = new IdentifierHelper();
		}
		return instance;
	}

	private IdentifierHelper() {
		classIdentifiersMap = new HashMap<>();
	}

	public int getIdentifierForClass(Class<?> clazz) {
		int identifier = 0;

		if (classIdentifiersMap.containsKey(clazz)) {
			identifier = classIdentifiersMap.get(clazz);
		}

		identifier++;

		classIdentifiersMap.put(clazz, identifier);

		return identifier;
	}

	public void setIdentifierForClass(Class<?> clazz, int identifier) {
		if (classIdentifiersMap.containsKey(clazz)) {
			int lastId = classIdentifiersMap.get(clazz);
			if (lastId < identifier) {
				classIdentifiersMap.put(clazz, identifier);
			}
		} else {
			classIdentifiersMap.put(clazz, identifier);
		}
	}

}
