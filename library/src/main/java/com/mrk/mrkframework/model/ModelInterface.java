package com.mrk.mrkframework.model;

import android.os.Parcelable;

public interface ModelInterface extends Parcelable {

	int getIdentifier();

	void setIdentifier(int identifier);

	ModelInterface copy();

}
