package com.mrk.mrkframework.viewmodel.abstracts;

import android.os.Parcel;

import com.mrk.mrkframework.model.ModelInterface;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public abstract class AbstractViewModel<T extends ModelInterface> implements IViewModel<T> {

	protected T data;

	@Override
	public T getData() {
		return data;
	}

	@Override
	public void loadFromData(T data) {
		this.data = (T) data.copy();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeParcelable(this.getData(), 0);
	}
}
