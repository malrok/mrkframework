package com.mrk.mrkframework.viewmodel.abstracts;

import android.content.Context;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.dataloaders.interfaces.IDataLoader;
import com.mrk.mrkframework.model.ModelInterface;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.OnListViewModelLoadingListener;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractListViewModel<T extends ModelInterface> implements IListViewModel<T> {

	private boolean isAsync = false;
	private boolean isLoading = false;
	private OnListViewModelLoadingListener listener;
	private List<IViewModel<T>> listData;
	private IViewModel<T> selectedItem;
	private DataSetObservable observable;

	// default constructor
	public AbstractListViewModel() {
		listData = new ArrayList<>();
		observable = new DataSetObservable();
	}

	@Override
	public void clear() {
		this.listData = new ArrayList<>();
	}

	@Override
	public List<T> getDataList() {
		List<T> data = new ArrayList<T>();
		for (IViewModel<T> vm : listData) {
			data.add(vm.getData());
		}
		return data;
	}

	public void loadFromData(Parcelable[] dataList, Class<? extends IViewModel<T>> viewModelClass) {
		for (Parcelable data : dataList) {
			IViewModel<T> vm = getViewModelInstance(viewModelClass);
			vm.loadFromData(((IViewModel<T>) data).getData());
			this.getViewModelList().add(vm);
		}
	}

	private IViewModel<T> getViewModelInstance(Class<? extends IViewModel<T>> viewModelClass) {
		IViewModel<T> vm = null;
		try {
			vm = viewModelClass.getConstructor().newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return vm;
	}

	@Override
	public List<IViewModel<T>> getViewModelList() {
		return this.listData;
	}
	
	@Override
	public IViewModel<T> getSelectedItem() {
		return selectedItem;
	}

	@Override
	public void setSelectedItem(IViewModel<T> viewmodel) {
		this.selectedItem = viewmodel;
	}

	@Override
	public void setSelectedItem(int rank) {
		if (rank >= 0 && rank < this.listData.size())
			this.selectedItem = this.listData.get(rank);
		else
			this.selectedItem = null;
	}

	@Override
	public void loadFromDataLoader(Context context, IDataLoader<T> dataLoader) {
		this.observable.notifyInvalidated();
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void addVM(IViewModel viewModel) {
		this.listData.add(viewModel);
		this.observable.notifyChanged();
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public void removeVM(IViewModel viewModel) {
		this.listData.remove(viewModel);
		this.observable.notifyChanged();
	}

	public boolean isAsync() {
		return this.isAsync;
	}
	
	public boolean isLoading() {
		return this.isLoading;
	}
	
	public void setOnListViewModelLoadingListener(OnListViewModelLoadingListener listener) {
		this.listener = listener;
	}

	public OnListViewModelLoadingListener getListener() {
		return this.listener;
	}

	public void registerObserver(DataSetObserver observer) {
		this.observable.registerObserver(observer);
	}

	public void unregisterObserver(DataSetObserver observer) {
		this.observable.unregisterObserver(observer);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		Parcelable[] list = new Parcelable[this.getViewModelList().size()];

		for (int rank = 0; rank < this.getViewModelList().size(); rank++) {
			Parcelable parcelable = this.getViewModelList().get(rank);
			list[rank] = parcelable;
		}

		parcel.writeParcelableArray(list, 0);
	}
}
