package com.mrk.mrkframework.viewmodel.interfaces;

public interface OnListViewModelLoadingListener {

	@SuppressWarnings("rawtypes")
	void finishedLoading(IListViewModel listViewModel);
	
}
