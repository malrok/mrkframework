package com.mrk.mrkframework.viewmodel.interfaces;

import java.util.List;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.Parcelable;

import com.mrk.mrkframework.dataloaders.interfaces.IDataLoader;
import com.mrk.mrkframework.model.ModelInterface;

public interface IListViewModel<T extends ModelInterface> extends Parcelable {

	void clear();

	void loadFromDataLoader(Context context, IDataLoader<T> dataLoader);

	void loadFromData(Parcelable[] dataList, Class<? extends IViewModel<T>> viewModelClass);

	List<T> getDataList();
	
	List<IViewModel<T>> getViewModelList();
	
	IViewModel<T> getSelectedItem();
	
	void setSelectedItem(IViewModel<T> viewmodel);
	
	void setSelectedItem(int rank);

	void addVM(IViewModel<T> viewModel);

	void removeVM(IViewModel<T> viewModel);

	IListViewModel<T> copy();

	void registerObserver(DataSetObserver observer);

	void unregisterObserver(DataSetObserver observer);
}
