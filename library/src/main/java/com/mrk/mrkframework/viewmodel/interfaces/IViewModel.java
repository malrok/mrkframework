package com.mrk.mrkframework.viewmodel.interfaces;

import android.os.Parcelable;

import com.mrk.mrkframework.model.ModelInterface;


public interface IViewModel<T extends ModelInterface> extends Parcelable {
	
	T getData();
	
	void loadFromData(T data);

	IViewModel<T> copy();
}
