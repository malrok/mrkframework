package com.mrk.mrkframework.dataloaders.abstracts;

import java.util.List;

import android.content.Context;

import com.mrk.mrkframework.dataloaders.interfaces.IDataParser;
import com.mrk.mrkframework.model.ModelInterface;

/**
 * universal xml parser
 */
public abstract class AbstractFileParser<T extends ModelInterface> implements IDataParser<T> {

	protected String file;
	
	public abstract List<T> parse(Context context);
	
	public abstract void unparse(Context context, List<T> objects);
	
}
