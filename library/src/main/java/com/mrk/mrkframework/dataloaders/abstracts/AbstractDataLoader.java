package com.mrk.mrkframework.dataloaders.abstracts;

import java.util.List;

import android.content.Context;

import com.mrk.mrkframework.dataloaders.interfaces.IDataLoader;
import com.mrk.mrkframework.model.ModelInterface;

public abstract class AbstractDataLoader<T extends ModelInterface> implements IDataLoader<T> {

	public abstract List<T> getData(Context context);
	
	public abstract void pushFrom(Context context, List<T> data);
}
