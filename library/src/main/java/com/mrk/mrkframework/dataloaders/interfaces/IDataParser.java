package com.mrk.mrkframework.dataloaders.interfaces;

import java.util.List;

import android.content.Context;

public interface IDataParser<T> {

	List<T> parse(Context context);
	
	void unparse(Context context, List<T> objects);
	
}
