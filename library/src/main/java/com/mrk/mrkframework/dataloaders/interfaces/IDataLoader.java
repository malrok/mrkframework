package com.mrk.mrkframework.dataloaders.interfaces;

import java.util.List;

import android.content.Context;


public interface IDataLoader<T> {

	List<T> getData(Context context);
	
	void pushFrom(Context context, List<T> data);
	
}
