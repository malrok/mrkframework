package com.mrk.mrkframework.ui.adapter.interfaces;

import android.widget.ListAdapter;

public interface ListFragmentAdapter extends ListAdapter {

	int getItemLayout();

}
