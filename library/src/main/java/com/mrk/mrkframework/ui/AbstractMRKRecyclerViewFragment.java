package com.mrk.mrkframework.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.mrk.mrkframework.databinding.AttributeParser;
import com.mrk.mrkframework.model.ModelInterface;
import com.mrk.mrkframework.ui.adapter.interfaces.AdapterLifeCycleInterface;
import com.mrk.mrkframework.ui.adapter.interfaces.RecyclerViewFragmentAdapter;
import com.mrk.mrkframework.ui.interfaces.MRKAdaptableListFragment;
import com.mrk.mrkframework.ui.interfaces.MRKListFragment;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public abstract class AbstractMRKRecyclerViewFragment<T extends ModelInterface> extends Fragment implements MRKListFragment, MRKAdaptableListFragment, AdapterView.OnItemClickListener {

	private View root;
	private IListViewModel<T> listViewModel;
	private RecyclerViewFragmentAdapter adapter;
	private RecyclerView recycler;
	private View emptyView;
	private AdapterLifeCycleInterface adapterCallback;

	private RecyclerView.AdapterDataObserver emptyObserver = new RecyclerView.AdapterDataObserver() {

		@Override
		public void onChanged() {
			RecyclerView.Adapter<?> adapter = recycler.getAdapter();
			if (adapter != null) {
				onEmptyAdapterChange(adapter.getItemCount() == 0);
			}
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		AttributeParser attributeParser = new AttributeParser();

		LayoutInflater layoutInflater = attributeParser.getLayoutInflater(inflater);
		root = layoutInflater.inflate(this.getLayout(), null);
		attributeParser.setViewAttribute(root);

		recycler = (RecyclerView) root.findViewById(android.R.id.list);

		if (recycler == null) {
			Log.e(this.getClass().getSimpleName(), "This requires to have a layout including a RecyclerView with id 'android.R.id.list'");
		} else {
			recycler.setLayoutManager(getLayoutManager());
		}

		emptyView = root.findViewById(android.R.id.empty);

		if (savedInstanceState != null) {
			this.listViewModel = savedInstanceState.getParcelable("viewModel");
		}

		return root;
	}

	@Override
	public void onStart() {
		super.onStart();

		if (this.listViewModel != null) {
			setAdapter();
		}
	}

	@Override
	public void onStop() {
		this.adapterCallback = null;
		if (this.adapter != null) {
			this.adapter.setOnItemClickListener(null);
			((RecyclerView.Adapter) this.adapter).unregisterAdapterDataObserver(emptyObserver);
			this.adapter.destroy();
		}
		super.onStop();
	}

	public RecyclerView.LayoutManager getLayoutManager() {
		return new LinearLayoutManager(this.getActivity());
	}

	public void setAdapterCallback(AdapterLifeCycleInterface adapterCallback) {
		this.adapterCallback = adapterCallback;
	}

	private void setAdapter() {
		Constructor<?> constructor = null;
		try {
			constructor = this.getAdapterClass().getConstructor(IListViewModel.class);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

		if (constructor != null) {
			try {
				this.adapter = (RecyclerViewFragmentAdapter) constructor.newInstance(this.listViewModel);
			} catch (java.lang.InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}

		if (adapter instanceof RecyclerView.Adapter) {
			RecyclerView.Adapter recyclerAdapter = (RecyclerView.Adapter) adapter;
			this.recycler.setAdapter(recyclerAdapter);
			this.adapter.setOnItemClickListener(this);
			recyclerAdapter.registerAdapterDataObserver(emptyObserver);
			onEmptyAdapterChange(recyclerAdapter.getItemCount() == 0);
		} else {
			Log.e(this.getClass().getSimpleName(), "Adapter must extend RecyclerView.Adapter");
		}

		if (this.adapterCallback != null) {
			adapterCallback.onAdapterSet(this);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		listViewModel.setSelectedItem(position);
	}

	@Override
	public List<T> getDataList() {
		return this.listViewModel.getDataList();
	}

	@Override
	public IListViewModel<T> getListViewModel() {
		return this.listViewModel;
	}

	@Override
	public void setListViewModel(IListViewModel listViewModel) {
		if (copyViewModel()) {
			this.listViewModel = (IListViewModel<T>) listViewModel.copy();
		} else {
			this.listViewModel = (IListViewModel<T>) listViewModel;
		}
		if (getActivity() != null) {
			setAdapter();
		}
	}

	public View getRootView() {
		return root;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putParcelable("viewModel", this.listViewModel);
	}

	protected void onEmptyAdapterChange(boolean isEmpty) {
		if (emptyView != null) {
			emptyView.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
			recycler.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Override if needed.
	 * @return returns true if the given view model should be copied before working on it
	 */
	protected boolean copyViewModel() {
		return true;
	}

	protected RecyclerView getRecycler() {
		return recycler;
	}
}
