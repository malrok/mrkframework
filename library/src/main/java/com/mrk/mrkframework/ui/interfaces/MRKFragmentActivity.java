package com.mrk.mrkframework.ui.interfaces;

import com.mrk.mrkframework.ui.fragmentdelegate.interfaces.FragmentActivityDelegate;

import android.app.Fragment;

import java.util.List;

public interface MRKFragmentActivity
{
	Class<? extends Fragment>[] getFragmentsList();
	
	void addFragmentChild(Class<? extends Fragment> fragment);
	
	FragmentActivityDelegate setDelegate();
	
	void sendActionToDelegate(String action);
}
