package com.mrk.mrkframework.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;

import com.mrk.mrkframework.ui.fragmentdelegate.interfaces.FragmentActivityDelegate;
import com.mrk.mrkframework.ui.interfaces.MRKFragmentActivity;


public abstract class AbstractMRKFragmentActivity extends AppCompatActivity implements MRKFragmentActivity {
	
	private FragmentActivityDelegate delegate;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.delegate = setDelegate();

		initializeFragments();
	}

	@Override
	protected void onDestroy() {
		this.delegate.destroy();

		super.onDestroy();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		Bundle delegateState = this.delegate.onSaveInstanceState(outState);
		super.onSaveInstanceState(delegateState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		initializeFragments();
		this.delegate.onRestoreInstanceState(savedInstanceState);

		super.onRestoreInstanceState(savedInstanceState);
	}

	private void initializeFragments() {
		for (Class<? extends Fragment> fragment : getFragmentsList()) {
			addFragmentChild(fragment);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		 if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (delegate != null) {
				try {
					delegate.onBackPressed();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		    return false;
		 }
		 return super.onKeyDown(keyCode, event);
	}

	@Override
	public void addFragmentChild(Class<? extends Fragment> fragment) {
		this.delegate.addFragmentChild(fragment);
	}

	@Override
	public void sendActionToDelegate(String action) {
		this.delegate.processAction(action);
	}
	
	public Fragment getCurrentFragment() {
		return this.delegate.getCurrentFragment();
	}
	
	public void setPreviousFragment() {
		this.delegate.setPreviousFragment();
	}
	
}
