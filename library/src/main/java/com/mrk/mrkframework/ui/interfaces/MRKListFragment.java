package com.mrk.mrkframework.ui.interfaces;

import com.mrk.mrkframework.model.ModelInterface;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;

import java.util.List;

public interface MRKListFragment<T extends ModelInterface> {

	int getLayout();

	List<T> getDataList();

	IListViewModel<T> getListViewModel();

	void setListViewModel(IListViewModel<T> listViewModel);
}
