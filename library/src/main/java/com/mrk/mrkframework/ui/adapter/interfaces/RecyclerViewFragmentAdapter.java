package com.mrk.mrkframework.ui.adapter.interfaces;

import android.widget.AdapterView;

public interface RecyclerViewFragmentAdapter {

	void destroy();

	int getItemLayout(int viewType);

	void setOnItemClickListener(AdapterView.OnItemClickListener listener);

}
