package com.mrk.mrkframework.ui.fragmentdelegate;

import com.mrk.mrkframework.ui.fragmentdelegate.interfaces.DelegateActions;

public abstract class AbstractDelegateActions implements DelegateActions {
	
	private String currentAction;
	
	@Override
	public void setAction(String action) {
		currentAction = action;
	}

	@Override
	public String getAction() {
		return currentAction;
	}

}
