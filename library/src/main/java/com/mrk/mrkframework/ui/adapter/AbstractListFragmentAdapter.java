package com.mrk.mrkframework.ui.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.mrk.mrkframework.model.ModelInterface;
import com.mrk.mrkframework.ui.adapter.interfaces.ListFragmentAdapter;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;

public abstract class AbstractListFragmentAdapter<ITEM extends ModelInterface> extends ArrayAdapter<ITEM> implements ListFragmentAdapter {

	private Context context;
	private IListViewModel<ITEM> vmList;
	private DataSetObserver observer = new DataSetObserver() {
		@Override
		public void onChanged() {
			AbstractListFragmentAdapter.this.notifyDataSetChanged();
		}

		@Override
		public void onInvalidated() {
			AbstractListFragmentAdapter.this.notifyDataSetInvalidated();
		}
	};

	public AbstractListFragmentAdapter(Context context, int layout, IListViewModel<ITEM> vmList) {
		super(context, layout);
		this.context = context;
		this.vmList = vmList;
		this.vmList.registerObserver(observer);
	}

	@Override
	public int getCount() {
		return vmList.getDataList().size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;

		// reuse views
		if (rowView == null) {
			LayoutInflater inflater = LayoutInflater.from(context);

			// configure view holder
			BindedViewHolder<IViewModel<ITEM>> viewHolder = new BindedViewHolder<IViewModel<ITEM>>(getItemLayout());
			viewHolder.inflateBinder(inflater);

			rowView = viewHolder.getRootView();
			rowView.setTag(viewHolder);
		}

		// fill data
		BindedViewHolder holder = (BindedViewHolder) rowView.getTag();
		holder.pushViewModel(vmList.getViewModelList().get(position));

		return rowView;
	}
}
