package com.mrk.mrkframework.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mrk.mrkframework.databinding.AttributeParser;
import com.mrk.mrkframework.databinding.binder.Binder;
import com.mrk.mrkframework.databinding.interfaces.BindedViewListener;
import com.mrk.mrkframework.model.ModelInterface;
import com.mrk.mrkframework.ui.interfaces.MRKFragment;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;

public abstract class AbstractMRKFragment<T extends ModelInterface> extends Fragment implements MRKFragment, BindedViewListener {

	private View root;
	private Binder binder;
	private IViewModel<T> viewModel;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		AttributeParser attributeParser = new AttributeParser();

		LayoutInflater layoutInflater = attributeParser.getLayoutInflater(inflater);
		root = layoutInflater.inflate(this.getLayout(), null);
		attributeParser.setViewAttribute(root);

		binder = new Binder(this, attributeParser.getAttributeList());

		if (savedInstanceState != null) {
			this.viewModel = savedInstanceState.getParcelable("viewModel");
		}

		return root;
	}

	@Override
	public void onStart() {
		super.onStart();

		binder.pushViewModel(viewModel);
	}

	@Override
	public IViewModel<T> getViewModel() {
		return viewModel;
	}

	@Override
	public void setViewModel(IViewModel viewmodel) {
		if (copyViewModel()) {
			this.viewModel = (IViewModel<T>) viewmodel.copy();
		} else {
			this.viewModel = (IViewModel<T>) viewmodel;
		}
	}

	@Override
	public View getRootView() {
		return root;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putParcelable("viewModel", viewModel);

		super.onSaveInstanceState(outState);
	}

	/**
	 * Override if needed.
	 * @return returns true if the given view model should be copied before working on it
	 */
	protected boolean copyViewModel() {
		return true;
	}

	@Override
	public void onValueChanged(View view, String bindValue, Object newValue, Object oldValue) {
		// nothing to do
	}
}
