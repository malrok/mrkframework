package com.mrk.mrkframework.ui.fragmentdelegate.interfaces;

public interface DelegateActions {

	void setAction(String action);
	
	String getAction();
	
}
