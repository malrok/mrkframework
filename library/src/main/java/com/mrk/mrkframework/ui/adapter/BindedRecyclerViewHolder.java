package com.mrk.mrkframework.ui.adapter;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mrk.mrkframework.R;
import com.mrk.mrkframework.databinding.AttributeParser;
import com.mrk.mrkframework.databinding.binder.Binder;
import com.mrk.mrkframework.databinding.interfaces.BindedViewListener;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;

public class BindedRecyclerViewHolder<VM extends IViewModel> extends RecyclerView.ViewHolder implements BindedViewListener {

	public static final String LIST_ITEM_CHANGED = "list_item_changed";
	public static final String EXTRA_POSITION = "position";
	public static final String EXTRA_BINDVALUE = "bindValue";
	public static final String EXTRA_NEWVALUE = "newValue";

	private AttributeParser attributeParser;
	private Binder binder;
	private View root;

	public BindedRecyclerViewHolder(View root, AttributeParser attributeParser) {
		super(root);
		this.root = root;
		this.root.setTag(R.string.HOLDER, this);
		this.attributeParser = attributeParser;
	}

	public void inflateBinder() {
		binder = new Binder(this, attributeParser.getAttributeList());
	}

	public void pushViewModel(VM vm) {
		binder.pushViewModel(vm);
	}

	public void setPositionInList(int positionInList) {
		this.root.setTag(R.string.POSITION, positionInList);
	}

	@Override
	public void onValueChanged(View view, String bindValue, Object newValue, Object oldValue) {
		Intent intent = new Intent(LIST_ITEM_CHANGED);

		if (this.root.getTag(R.string.POSITION) != null) {
			intent.putExtra(EXTRA_POSITION, Integer.parseInt(this.root.getTag(R.string.POSITION).toString()));
		}

		intent.putExtra(EXTRA_BINDVALUE, bindValue);
		intent.putExtra(EXTRA_NEWVALUE, newValue.toString());

		LocalBroadcastManager.getInstance(view.getContext()).sendBroadcast(intent);
	}

	@Override
	public View getRootView() {
		return root;
	}

}
