package com.mrk.mrkframework.ui.interfaces;

public interface MRKAdaptableListFragment {

	int getItemLayout();

	Class<?> getAdapterClass();

}
