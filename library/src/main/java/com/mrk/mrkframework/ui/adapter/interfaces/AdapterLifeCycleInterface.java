package com.mrk.mrkframework.ui.adapter.interfaces;

import android.app.Fragment;

public interface AdapterLifeCycleInterface {

	void onAdapterSet(Fragment fragment);

}
