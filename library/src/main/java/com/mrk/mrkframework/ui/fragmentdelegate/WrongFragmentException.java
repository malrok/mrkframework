package com.mrk.mrkframework.ui.fragmentdelegate;

public class WrongFragmentException extends Exception {

	private static final long serialVersionUID = 6404471940326104776L;
	
	public WrongFragmentException(String message) {
		super(message);
	}

}
