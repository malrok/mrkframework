package com.mrk.mrkframework.ui;

import android.app.Fragment;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mrk.mrkframework.databinding.AttributeParser;
import com.mrk.mrkframework.model.ModelInterface;
import com.mrk.mrkframework.ui.interfaces.MRKListFragment;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;

import java.util.List;

public abstract class AbstractMRKListFragment<T extends ModelInterface> extends Fragment implements MRKListFragment {

	private View root;
	protected IListViewModel<T> listViewModel;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		AttributeParser attributeParser = new AttributeParser();

		LayoutInflater layoutInflater = attributeParser.getLayoutInflater(inflater);
		root = layoutInflater.inflate(this.getLayout(), null);
		attributeParser.setViewAttribute(root);

		if (savedInstanceState != null) {
			this.listViewModel = savedInstanceState.getParcelable("viewModel");
		}

		return root;
	}

	@Override
	public List<T> getDataList() {
		return this.listViewModel.getDataList();
	}

	@Override
	public IListViewModel<T> getListViewModel() {
		return this.listViewModel;
	}

	@Override
	public void setListViewModel(IListViewModel listViewModel) {
		if (copyViewModel()) {
			this.listViewModel = (IListViewModel<T>) listViewModel.copy();
		} else {
			this.listViewModel = (IListViewModel<T>) listViewModel;
		}
	}

	public View getRootView() {
		return root;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putParcelable("viewModel", this.listViewModel);
	}

	/**
	 * Override if needed.
	 * @return returns true if the given view model should be copied before working on it
	 */
	protected boolean copyViewModel() {
		return true;
	}

}
