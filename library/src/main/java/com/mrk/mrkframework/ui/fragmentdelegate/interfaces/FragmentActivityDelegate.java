package com.mrk.mrkframework.ui.fragmentdelegate.interfaces;

import android.app.Fragment;
import android.os.Bundle;


public interface FragmentActivityDelegate {

	void destroy();

	void onBackPressed();
	
	void setPreviousFragment();
	
	/**
	 * Takes an action and returns the name of the fragment to display
	 * @param action
	 */
	void processAction(String action);

	Fragment getFragmentFromAction(String action);
	
	void addFragmentChild(Class<? extends Fragment> fragment);
	
	Fragment getCurrentFragment();

	Bundle onSaveInstanceState(Bundle outState);

	void onRestoreInstanceState(Bundle savedInstanceState);
}
