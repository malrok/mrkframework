package com.mrk.mrkframework.ui.fragmentdelegate;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.mrk.mrkframework.ui.fragmentdelegate.interfaces.FragmentActivityDelegate;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractFragmentActivityDelegate implements FragmentActivityDelegate {

	private Map<String, WeakReference<Fragment>> fragmentsList;
	private WeakReference<Activity> activity;
	private int fragmentContainer = -1;

	private String previousFragment;
	protected WeakReference<Fragment> currentFragment;
	
	public AbstractFragmentActivityDelegate(Activity activity) {
		this.activity = new WeakReference<>(activity);

		fragmentsList = new HashMap<>();
	}
	
	public AbstractFragmentActivityDelegate(Activity activity, int fragmentContainer) {
		this(activity);
		this.fragmentContainer = fragmentContainer;
	}

	public void destroy() {
		this.fragmentsList.clear();
	}

	public void addFragmentChild(Class<? extends Fragment> fragment) {
		fragmentsList.put(fragment.getName(), null);
	}
	
	public Fragment getCurrentFragment() {
		return this.currentFragment == null ? null : this.currentFragment.get();
	}
	
	protected Activity getActivity() {
		return activity.get();
	}

	protected void changeFragment(Fragment fragment) {
		changeFragment(fragment, this.fragmentContainer);
	}
	
	protected void changeFragment(Fragment fragment, int fragmentContainer) {
		changeFragment(fragment, fragmentContainer, false);
	}
	
	protected void changeFragment(Fragment fragment, boolean addToBackStack) {
		changeFragment(fragment, fragmentContainer, addToBackStack);
	}
	
	protected void changeFragment(Fragment fragment, int fragmentContainer, boolean addToBackStack) {
		if (fragment != null && fragmentContainer != -1) {
			Activity currentActivity = activity.get();

			Fragment currFrag = getCurrentFragment();

			if (currFrag != null) {
				previousFragment = currFrag.getClass().getName();
			}

			FragmentTransaction ft = currentActivity.getFragmentManager().beginTransaction();
			ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

			currentActivity.getFragmentManager().popBackStack();
			
			if (addToBackStack) {
				ft.addToBackStack(null);
			}

			ft.replace(fragmentContainer, fragment);
			ft.commit();

			currentFragment = new WeakReference<>(fragment);
		}
	}
	
	protected Fragment getFragmentFromClassName(String name) throws WrongFragmentException {
		if (fragmentsList.containsKey(name)) {
			if (fragmentsList.get(name) == null || fragmentsList.get(name).get() == null) {
				Fragment fragment;
				try {
					fragment = (Fragment) Class.forName(name).newInstance();
				} catch (InstantiationException e) {
					e.printStackTrace();
					return null;
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					return null;
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					return null;
				}
				fragmentsList.put(name, new WeakReference<>(fragment));
			}
			return fragmentsList.get(name).get();
		}
		
		throw new WrongFragmentException("Unknown fragment " + name);
	}

	@Override
	public void setPreviousFragment() {
		try {
			changeFragment(getFragmentFromClassName(previousFragment));
		} catch (WrongFragmentException e) {
			e.printStackTrace();
		}
	}

	public void processAction(String action) {
		Fragment nextFragment = getFragmentFromAction(action);

		Fragment currFrag = getCurrentFragment();

		if (nextFragment != null && (currFrag != null
				&& !nextFragment.getClass().getSimpleName().equals(currFrag.getClass().getSimpleName()))
				|| currFrag == null) {
			changeFragment(nextFragment);
		}
	}

	@Override
	public Bundle onSaveInstanceState(Bundle outState) {
		outState.putInt("container", fragmentContainer);
		outState.putString("previous", previousFragment);

		Fragment currFrag = getCurrentFragment();

		outState.putString("current", currFrag != null ? currFrag.getClass().getName() : "");

		return outState;
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		this.fragmentContainer = savedInstanceState.getInt("container");
		this.previousFragment = savedInstanceState.getString("previous");

		try {
			this.currentFragment = new WeakReference<>(getFragmentFromClassName(savedInstanceState.getString("current")));
		} catch (WrongFragmentException e) {
			e.printStackTrace();
		}
	}
}
