package com.mrk.mrkframework.ui;

import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mrk.mrkframework.databinding.AttributeParser;
import com.mrk.mrkframework.model.ModelInterface;
import com.mrk.mrkframework.ui.adapter.interfaces.AdapterLifeCycleInterface;
import com.mrk.mrkframework.ui.adapter.interfaces.ListFragmentAdapter;
import com.mrk.mrkframework.ui.interfaces.MRKAdaptableListFragment;
import com.mrk.mrkframework.ui.interfaces.MRKListFragment;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public abstract class AbstractMRKAdaptableListFragment<T extends ModelInterface> extends ListFragment implements MRKListFragment, MRKAdaptableListFragment {

	private View root;
	protected IListViewModel<T> listViewModel;

	private ListFragmentAdapter adapter;
	private AdapterLifeCycleInterface adapterCallback;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		AttributeParser attributeParser = new AttributeParser();

		LayoutInflater layoutInflater = attributeParser.getLayoutInflater(inflater);
		root = layoutInflater.inflate(this.getLayout(), null);
		attributeParser.setViewAttribute(root);

		if (savedInstanceState != null) {
			this.listViewModel = savedInstanceState.getParcelable("viewModel");
		}

		return root;
	}

	@Override
	public void onStop() {
		this.adapterCallback = null;
		super.onStop();
	}

	public void setAdapterCallback(AdapterLifeCycleInterface adapterCallback) {
		this.adapterCallback = adapterCallback;
	}

	private void setAdapter() {
		Constructor<?> constructor = null;
		try {
			constructor = this.getAdapterClass().getConstructor(Context.class, Integer.TYPE, IListViewModel.class);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

		if (constructor != null) {
			try {
				this.adapter = (ListFragmentAdapter) constructor.newInstance(getActivity().getApplicationContext(), getItemLayout(), this.listViewModel);
			} catch (java.lang.InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}

		this.setListAdapter(adapter);

		if (this.adapterCallback != null) {
			this.adapterCallback.onAdapterSet(this);
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		if (this.listViewModel != null) {
			setAdapter();
		}
	}

	@Override
	public List<T> getDataList() {
		return this.listViewModel.getDataList();
	}

	@Override
	public IListViewModel<T> getListViewModel() {
		return this.listViewModel;
	}

	@Override
	public void setListViewModel(IListViewModel listViewModel) {
		if (copyViewModel()) {
			this.listViewModel = (IListViewModel<T>) listViewModel.copy();
		} else {
			this.listViewModel = (IListViewModel<T>) listViewModel;
		}
		if (getActivity() != null) {
			setAdapter();
		}
	}

	@Override
	public void onListItemClick(ListView listView, View view, int position, long id) {
		this.listViewModel.setSelectedItem(position);
	}

	public View getRootView() {
		return root;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putParcelable("viewModel", this.listViewModel);
	}

	/**
	 * Override if needed.
	 * @return returns true if the given view model should be copied before working on it
	 */
	protected boolean copyViewModel() {
		return true;
	}
}
