package com.mrk.mrkframework.ui.adapter;

import android.database.DataSetObserver;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.mrk.mrkframework.R;
import com.mrk.mrkframework.databinding.AttributeParser;
import com.mrk.mrkframework.model.ModelInterface;
import com.mrk.mrkframework.ui.adapter.interfaces.RecyclerViewFragmentAdapter;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRecyclerViewFragmentAdapter<ITEM extends ModelInterface> extends RecyclerView.Adapter<BindedRecyclerViewHolder> implements RecyclerViewFragmentAdapter, View.OnClickListener {

	private IListViewModel<ITEM> vmList;
	private DataSetObserver observer = new DataSetObserver() {
		@Override
		public void onChanged() {
			AbstractRecyclerViewFragmentAdapter.this.notifyDataSetChanged();
		}

		@Override
		public void onInvalidated() {
			AbstractRecyclerViewFragmentAdapter.this.notifyDataSetChanged();
		}
	};

	private OnItemClickListener itemClickListener;
	private SparseBooleanArray selectedItems = new SparseBooleanArray();

	public AbstractRecyclerViewFragmentAdapter(IListViewModel<ITEM> vmList) {
		this.vmList = vmList;
		this.vmList.registerObserver(observer);
	}

	@Override
	public void destroy() {
		this.vmList.unregisterObserver(observer);
	}

	public void setOnItemClickListener(AdapterView.OnItemClickListener listener) {
		this.itemClickListener = listener;
	}

	@Override
	public BindedRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		AttributeParser attributeParser = new AttributeParser();
		LayoutInflater layoutInflater = attributeParser.getLayoutInflater(LayoutInflater.from(parent.getContext()));
		View root = layoutInflater.inflate(getItemLayout(viewType), parent, false);
		attributeParser.setViewAttribute(root);

		return getViewHolder(root, attributeParser);
	}

	@Override
	public void onBindViewHolder(BindedRecyclerViewHolder holder, int position) {
		holder.inflateBinder();
		holder.pushViewModel(this.vmList.getViewModelList().get(position));
		holder.setPositionInList(position);
		holder.getRootView().setOnClickListener(this);
		holder.itemView.setActivated(selectedItems.get(position, false));
	}

	@Override
	public int getItemCount() {
		return vmList.getDataList().size();
	}

	public BindedRecyclerViewHolder getViewHolder(View view, AttributeParser attributeParser) {
		return new BindedRecyclerViewHolder<>(view, attributeParser);
	}

	@Override
	public void onClick(View v) {
		if (itemClickListener != null && v.getTag(R.string.POSITION) != null) {
			int position = (int) v.getTag(R.string.POSITION);
			toggleSelection(position);
			itemClickListener.onItemClick(null, v, position, getItemId(position));
		}
	}

	public void toggleSelection(int pos) {
		if (selectedItems.get(pos, false)) {
			selectedItems.delete(pos);
		} else {
			selectedItems.put(pos, true);
		}
		notifyItemChanged(pos);

	}

	public void setSelected(int pos) {
		selectedItems.put(pos, true);
		notifyItemChanged(pos);

	}

	public void clearSelection(int pos) {
		if (selectedItems.get(pos, false)) {
			selectedItems.delete(pos);
		}
		notifyItemChanged(pos);
	}

	public void clearSelections() {
		if (selectedItems.size() > 0) {
			selectedItems.clear();
			notifyDataSetChanged();
		}
	}


	public int getSelectedItemCount() {
		return selectedItems.size();
	}

	public List<Integer> getSelectedItemsPositions() {
		List<Integer> items = new ArrayList<Integer>(selectedItems.size());
		for (int i = 0; i < selectedItems.size(); i++) {
			items.add(selectedItems.keyAt(i));
		}
		return items;
	}
}
