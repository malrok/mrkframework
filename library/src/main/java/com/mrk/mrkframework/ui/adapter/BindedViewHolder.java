package com.mrk.mrkframework.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;

import com.mrk.mrkframework.databinding.AttributeParser;
import com.mrk.mrkframework.databinding.binder.Binder;
import com.mrk.mrkframework.databinding.interfaces.BindedViewListener;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;

public class BindedViewHolder<VM extends IViewModel> implements BindedViewListener {

	private AttributeParser attributeParser = new AttributeParser();
	private Binder binder;
	private int layout;
	private View root;

	public BindedViewHolder(int layout) {
		this.layout = layout;
	}

	public void inflateBinder(LayoutInflater inflater) {
		LayoutInflater layoutInflater = attributeParser.getLayoutInflater(inflater);
		root = layoutInflater.inflate(this.layout, null);
		attributeParser.setViewAttribute(root);

		binder = new Binder(this, attributeParser.getAttributeList());
	}

	public void pushViewModel(VM vm) {
		binder.pushViewModel(vm);
	}

	@Override
	public void onValueChanged(View view, String bindValue, Object newValue, Object oldValue) {
		// TODO
	}

	@Override
	public View getRootView() {
		return root;
	}
}
