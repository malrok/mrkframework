package com.mrk.mrkframework.ui.interfaces;

import com.mrk.mrkframework.model.ModelInterface;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;

public interface MRKFragment<T extends ModelInterface> {

	int getLayout();

	IViewModel<T> getViewModel();

	void setViewModel(IViewModel<T> viewmodel);

}
